package routes

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/example-discord-bot/controllers"
)

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	router.HandleFunc("/example/status", c.GetStatus).Methods("GET")
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router, port string) {
	listenerPort := fmt.Sprintf(":%s", port)
	log.Fatal(http.ListenAndServe(listenerPort, router))
}
