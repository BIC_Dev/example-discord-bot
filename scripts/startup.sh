export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export DISCORD_TOKEN=$(echo ${ENV_VARS} | jq -r '.DISCORD_TOKEN')
export SERVICE_TOKEN=$(echo ${ENV_VARS} | jq -r '.SERVICE_TOKEN')
export LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.LOG_LEVEL')
export LISTENER_PORT="8080"

/bot